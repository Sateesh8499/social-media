import firebase from 'firebase';
const firebaseConfig = {
  apiKey: 'AIzaSyDMYj8OGLqi-0oZHZpB3awVAnR3laxmfIY',
  authDomain: 'social-media-app-a0484.firebaseapp.com',
  databaseURL: 'https://social-media-app-a0484.firebaseio.com',
  projectId: 'social-media-app-a0484',
  storageBucket: 'social-media-app-a0484.appspot.com',
  messagingSenderId: '523378611204',
  appId: '1:523378611204:web:c4a99858544b0300615151',
  measurementId: 'G-N3TWC754WF',
};
const fire = firebase.initializeApp(firebaseConfig);
firebase.analytics();
export const db = fire.firestore();
export const firebaseDatabaseRef = firebase.database();
export default fire;
