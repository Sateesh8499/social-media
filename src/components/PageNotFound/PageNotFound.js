import React from 'react';

const PageNotFound = () => {
  return (
    <div>
      <div className='display-1 text-center'>Page Not Found</div>
    </div>
  );
};

export default PageNotFound;
