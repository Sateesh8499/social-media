import React from 'react';
import Navbar from '../Navbar/Navbar';
import Home from '../Home/Home';
import classes from './Layout.module.css';

const Layout = () => {
  return (
    <div className={classes.Layout}>
      <Navbar />
      <Home />
    </div>
  );
};
export default Layout;
