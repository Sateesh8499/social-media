import React, { Component } from 'react';
import classes from './Register.module.css';
import fire, { db, firebaseDatabaseRef } from '../../config/Fire';
import { NavLink } from 'react-router-dom';
import { FiUserPlus } from 'react-icons/fi';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      photoURL: '',
      displayName: '',
      phoneNumber: '',
      havingError: false,
      user: null,
    };
  }
  handleInputChange = (event) => {
    this.setState({ havingError: false });
    this.setState({ [event.target.name]: event.target.value });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    fire
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(
        (res) => {
          res.user
            .updateProfile({
              displayName: this.state.displayName,
              photoURL: this.state.photoURL,
            })
            .then((result) => {});
          window.location.href = '/home';
        },
        (error) => {
          this.setState({ havingError: true });
        }
      );
  };
  render() {
    return (
      <div
        className={classes.Register}
        style={{ backgroundColor: 'rebeccapurple' }}
      >
        <form className={classes.Form} onSubmit={this.handleSubmit}>
          <div style={{ justifyContent: 'center', display: 'flex' }}>
            <div className={classes.UserIcon}>
              <FiUserPlus />
            </div>
          </div>
          <h2 className={`${classes.Header} 'text-center p-4'`}>Register</h2>
          {this.state.havingError ? (
            <div className={classes.havingError}>
              <span className='text-danger'>
                Please Enter Valid Email and Password.
              </span>
            </div>
          ) : null}
          <div className='form-group'>
            <label htmlFor='displayName'>User Name</label>
            <input
              type='text'
              name='displayName'
              value={this.state.displayName}
              onChange={this.handleInputChange}
              className='form-control'
              id='displayName'
              placeholder='User Name'
              autoComplete='true'
            />
          </div>
          <div className='form-group'>
            <label htmlFor='email'>Email</label>
            <input
              type='email'
              name='email'
              value={this.state.email}
              onChange={this.handleInputChange}
              className='form-control'
              id='email'
              placeholder='example@email.com'
              autoComplete='true'
            />
          </div>
          <div className='form-group'>
            <label htmlFor='password'>Password</label>
            <input
              type='password'
              name='password'
              value={this.state.password}
              onChange={this.handleInputChange}
              className='form-control'
              id='password'
              placeholder='Enter Password'
            />
          </div>
          <button type='submit'>Register</button>
          <div className={classes.DontHaveAcc}>
            <span>
              Have Account ? <br />
              <NavLink to='/login'>Login</NavLink>
            </span>
          </div>
        </form>
      </div>
    );
  }
}

export default Register;
