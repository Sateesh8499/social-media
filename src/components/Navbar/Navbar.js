import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { FiUser, FiHome, FiLogOut, FiEdit3 } from 'react-icons/fi';
import fire, { firebaseDatabaseRef } from '../../config/Fire';
import classes from './Navbar.module.css';

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: {},
    };
  }

  handleSignOut = () => {
    fire.auth().signOut();
    window.location.href = '/login';
  };
  componentWillMount() {
    this.getCurrentUser();
  }
  getCurrentUser = () => {
    const uid = fire.auth().currentUser.uid;
    const ref = firebaseDatabaseRef
      .ref('users/' + uid)
      .on('value', (snapshot) => {
        const user = snapshot.val();
        if (user !== null) {
          const userData = Object.keys(user);
          this.setState({ postKey: userData });
          firebaseDatabaseRef
            .ref('users/' + uid + '/' + userData)
            .on('value', (data) => {
              const newUser = data.val();
              this.setState({ currentUser: newUser, userKey: userData });
              localStorage.setItem(
                'currentUser',
                JSON.stringify(fire.auth().currentUser)
              );
              localStorage.setItem(
                'userKey',
                JSON.stringify(this.state.userKey)
              );
            });
        }
      });
  };
  render() {
    return (
      <div>
        <nav className='navbar navbar-expand-lg navbar-dark bg-dark d-flex justify-content-between align-items-center'>
          {/* <NavLink className='navbar-brand' to='/home'>
            Logo
          </NavLink> */}
          <div className={`${classes.NavBarList} navbar`}>
            <ul className='navbar-nav'>
              <li className='nav-item'>
                <NavLink exact to='/home'>
                  <FiHome /> <span>Home</span>
                </NavLink>
              </li>
            </ul>
          </div>
          <div className={`${classes.ToggleBtn} btn-group`}>
            <button
              type='button'
              className='btn btn-dark dropdown-toggle'
              data-toggle='dropdown'
              aria-haspopup='true'
              aria-expanded='false'
            >
              {fire.auth().currentUser.photoURL ? (
                <img
                  src={fire.auth().currentUser.photoURL}
                  style={{
                    width: '40px',
                    height: '40px',
                    marginRight: '5px',
                    borderRadius: '50%',
                  }}
                  alt=''
                />
              ) : (
                <FiUser />
              )}
              <span className='ml-1'>
                {fire.auth().currentUser.displayName}
              </span>
            </button>
            <div
              className={`${classes.DropdownMenu} dropdown-menu dropdown-menu-right`}
            >
              <NavLink type='button' exact to='/profile'>
                <FiUser /> View Profile
              </NavLink>
              <NavLink type='button' exact to='/update'>
                <FiEdit3 /> Update Profile
              </NavLink>
              <hr />
              <button onClick={this.handleSignOut} type='button'>
                <FiLogOut /> Logout
              </button>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;
