import React from 'react';
import classes from './Spinner.module.css';

const Spinner = (props) => {
  return (
    <div>
      {props.loader ? (
        <div className={classes.LoaderBackground}>
          <div className={classes.Loader}>Loading...</div>
        </div>
      ) : null}
    </div>
  );
};

export default Spinner;
