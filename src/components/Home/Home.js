import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import UserProfile from '../User/UserProfile/UserProfile';
import Posts from '../User/Posts/Posts';
import UpdateProfile from '../User/UpdateProfile/UpdateProfile';

class Home extends Component {
  render() {
    return (
      <Switch>
        <Route exact path='/home' component={Posts} />
        <Route path='/profile' component={UserProfile} />
        <Route path='/update' component={UpdateProfile} />
      </Switch>
    );
  }
}

export default Home;
