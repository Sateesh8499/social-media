import React, { Component } from 'react';
import fire, { firebaseDatabaseRef } from '../../../config/Fire';
import classes from './UpdateProfile.module.css';
import { FiUser, FiUpload } from 'react-icons/fi';
import firebase from 'firebase';
class UpdateProfile extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      photoURL: '',
      displayName: '',
      phoneNumber: '',
      isProfileUploaded: false,
      fileName: '',
      isUserNameAndEtcUpdated: false,
    };
  }
  componentDidMount() {
    this._isMounted = true;
    this.authListener();
    this.getUserProfile();
  }
  componentWillUnmount() {
    this._isMounted = false;
  }

  authListener = () => {
    fire.auth().onAuthStateChanged((user) => {
      if (user) {
        fire
          .storage()
          .ref('users/' + fire.auth().currentUser.uid + '/profile.jpg')
          .getDownloadURL()
          .then((imgURL) => {
            fire
              .auth()
              .currentUser.updateProfile({
                photoURL: imgURL,
              })
              .then((res) => {});
          })
          .catch((error) => {});
      } else {
      }
    });
  };
  getUserProfile = () => {
    const currentUser = {
      uid: fire.auth().currentUser.uid,
      email: fire.auth().currentUser.email,
      password: fire.auth().currentUser.password,
      photoURL: fire.auth().currentUser.photoURL,
      displayName: fire.auth().currentUser.displayName,
      phoneNumber: fire.auth().currentUser.phoneNumber,
    };
    if (this._isMounted) {
      this.setState({ ...currentUser });
    }
  };

  handleInputChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    fire
      .auth()
      .currentUser.updateProfile({
        displayName: this.state.displayName,
        photoURL: this.state.photoURL,
      })
      .then((res) => {
        setTimeout(() => {
          this.setState({ isUserNameAndEtcUpdated: false });
        }, 2000);
        this.setState({ isUserNameAndEtcUpdated: true });
      })
      .catch((error) => {})
      .then(
        firebaseDatabaseRef
          .ref('/posts/' + fire.auth().currentUser.uid)
          .on('value', (snapshot) => {
            const oldPosts = Object.keys(snapshot.val());
            oldPosts.forEach((postKey) => {
              const db = firebaseDatabaseRef.ref(
                'posts/' + fire.auth().currentUser.uid + '/' + postKey
              );
              db.update({
                displayName: this.state.displayName,
                photoURL: this.state.photoURL,
              });
            });
          })
      )
      .catch((error) => {});
    fire
      .auth()
      .currentUser.updateEmail(this.state.email)
      .then((res) => {
        setTimeout(() => {
          this.setState({ isUserNameAndEtcUpdated: false });
        }, 2000);
        this.setState({ isUserNameAndEtcUpdated: true });
      })
      .catch((error) => {})
      .then(
        setTimeout(() => {
          window.location.href = '/profile';
        }, 3000)
      );
    if (this.state.password) {
      fire
        .auth()
        .currentUser.updatePassword(this.state.password)
        .then((res) => {
          setTimeout(() => {
            this.setState({ isUserNameAndEtcUpdated: false });
          }, 2000);
          this.setState({ isUserNameAndEtcUpdated: true });
        })
        .catch((error) => {});
    }
  };

  chooseFile = (e) => {
    this.setState({ fileName: e.target.value });
    this.file = e.target.files[0];
  };
  handleUpload = () => {
    fire
      .storage()
      .ref('users/' + fire.auth().currentUser.uid + '/profile.jpg')
      .put(this.file)
      .then((res) => {
        setTimeout(() => {
          this.setState({ isProfileUploaded: false });
        }, 3000);
        this.setState({ isProfileUploaded: true });
      })
      .then(this.authListener())
      .catch((error) => {});
  };
  render() {
    return (
      <div className={classes.UpdateProfile}>
        <div className={classes.Card}>
          <div className={classes.Img}>
            {this.state.isProfileUploaded ? (
              <div className={classes.Alert} role='alert'>
                <strong>Profile Picture Updated Successfully.</strong>
              </div>
            ) : null}
            {this.state.isUserNameAndEtcUpdated ? (
              <div className={classes.Alert} role='alert'>
                <strong>Profile Updated Successfully.</strong>
              </div>
            ) : null}
            {this.state.photoURL ? (
              <label htmlFor='profile' className='btn'>
                <img src={this.state.photoURL} alt='Profile' />
              </label>
            ) : (
              <label htmlFor='profile' className='btn'>
                <div className={classes.UserIcon}>
                  <FiUser />
                </div>
              </label>
            )}
          </div>
          <div>{this.state.fileName}</div>
          <div className={classes.ChooseFile}>
            <input
              id='profile'
              type='file'
              onChange={(e) => this.chooseFile(e)}
              className={classes.ChooseFile}
              placeholder='Choose File'
            />
            <button
              onClick={this.handleUpload}
              className={classes.UploadBtn}
              type='button'
              disabled={!this.file}
            >
              <FiUpload /> Upload
            </button>
          </div>
        </div>
        <form className={classes.CardBody} onSubmit={this.handleSubmit}>
          <div className='form-group'>
            <label htmlFor='displayName'>User Name</label>
            <input
              type='text'
              name='displayName'
              value={this.state.displayName || ''}
              onChange={this.handleInputChange.bind(this)}
              id='displayName'
              placeholder='User Name'
              autoComplete='true'
            />
          </div>
          <div className='form-group'>
            <label htmlFor='email'>Email</label>
            <input
              type='email'
              name='email'
              value={this.state.email || ''}
              onChange={this.handleInputChange.bind(this)}
              id='email'
              placeholder='example@email.com'
              autoComplete='true'
            />
          </div>
          <div className='form-group'>
            <label htmlFor='password'>Password</label>
            <input
              type='password'
              name='password'
              value={this.state.password || ''}
              onChange={this.handleInputChange.bind(this)}
              id='password'
              placeholder='Enter Password'
            />
            <label className='text-danger'>
              If user change Password. The user must sign in again.
            </label>
          </div>
          <div className={classes.UpdateBtn}>
            <button className='btn btn-outline-primary' type='submit'>
              Update
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default UpdateProfile;
