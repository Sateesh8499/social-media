import React, { useState, useEffect, Component } from 'react';
import fire, { firebaseDatabaseRef } from '../../../config/Fire';
import classes from './UserProfile.module.css';
import Post from '../Posts/Post/Post';
import { FiUser, FiEdit3 } from 'react-icons/fi';
import { NavLink } from 'react-router-dom';

class UserProfile extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      currentUser: {
        email: null,
        password: null,
        photoURL: null,
        displayName: null,
        phoneNumber: null,
      },
      posts: {},
    };
  }
  signOut = () => {
    fire.auth().signOut();
  };
  componentDidMount() {
    this._isMounted = true;
    firebaseDatabaseRef
      .ref('/posts/' + fire.auth().currentUser.uid)
      .on('value', (snapshot) => {
        const oldPosts = snapshot.val();
        if (this._isMounted) {
          this.setState({ posts: oldPosts });
        }
      });
    this.getUserProfile();
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  // getUserPosts = () => {
  //   firebaseDatabaseRef
  //     .ref('/posts/' + fire.auth().currentUser.uid)
  //     .on('value', (snapshot) => {
  //       this.setState({ posts: { ...snapshot.val() } });
  //     });
  // };
  getUserProfile = () => {
    this.setState({ currentUser: { ...fire.auth().currentUser } });
  };
  handleRemovePost = (id) => {
    const dataRef = firebaseDatabaseRef.ref(
      '/posts/' + fire.auth().currentUser.uid + '/' + id
    );
    dataRef.remove();
  };
  render() {
    return (
      <div className={classes.UserProfile}>
        <div className={classes.Card}>
          <div className={classes.Img}>
            {this.state.currentUser.photoURL ? (
              <img src={this.state.currentUser.photoURL} alt='' />
            ) : (
              <div className={classes.UserIcon}>
                <FiUser />
              </div>
            )}
          </div>
          <div className={classes.CardBody}>
            <h4 className='card-title'>{this.state.currentUser.displayName}</h4>
            <p className='card-text'>{this.state.currentUser.email}</p>
          </div>
          <div className={classes.UpdateBtnDiv}>
            <NavLink
              className={`${classes.UpdateBtn} btn btn-outline-primary`}
              to='/update'
            >
              <FiEdit3 /> Update Profile
            </NavLink>
          </div>
          <hr />
        </div>

        <div className={classes.TimeLine}>
          <h4 className='text-left'>My Posts</h4>
          {this.state.posts ? (
            Object.keys(this.state.posts).map((pos, i) => {
              return (
                <Post
                  key={pos}
                  showRemoveBtn={true}
                  timeAndDate={this.state.posts[pos].postTime}
                  profilePic={this.state.posts[pos].photoURL}
                  displayName={this.state.posts[pos].displayName}
                  post={this.state.posts[pos].post}
                  removePost={this.handleRemovePost.bind(this, pos)}
                />
              );
            })
          ) : (
            <div className={classes.NoPostsCard}>--- No Posts ---</div>
          )}
        </div>
      </div>
    );
  }
}
export default UserProfile;
