import React, { Component } from 'react';
import classes from './Post.module.css';
import { FiTrash, FiClock, FiUser, FiHeart } from 'react-icons/fi';
import { FaHeart } from 'react-icons/fa';
class Post extends Component {
  render() {
    return (
      <div className={classes.Post} onDoubleClick={this.props.click}>
        <div className={classes.FirstDiv}>
          {this.props.profilePic ? (
            <img src={this.props.profilePic} alt='ProPic' />
          ) : (
            <div className={classes.Fiuser}>
              <FiUser />
            </div>
          )}
        </div>
        <div className={classes.SecondDiv}>
          <span>
            <FiClock />
            <small>{this.props.timeAndDate}</small>
          </span>
          <h4>{this.props.displayName}</h4>
          <p>{this.props.post}</p>
        </div>

        {this.props.showRemoveBtn ? (
          <button className={classes.TrashBtn} onClick={this.props.removePost}>
            <FiTrash />
          </button>
        ) : null}
      </div>
    );
  }
}
export default Post;
