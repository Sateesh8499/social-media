import React, { Component } from 'react';
import classes from './Posts.module.css';
import fire, { firebaseDatabaseRef } from '../../../config/Fire';
import Post from './Post/Post';
import { GiPaperPlane } from 'react-icons/gi';

class Posts extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      userKey: null,
      postKey: null,
      newPost: '',
      posts: {},
    };
  }
  componentDidMount() {
    this._isMounted = true;
    this.getPosts();
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  getPosts = () => {
    firebaseDatabaseRef
      .ref('posts/')
      .orderByChild('postTime')
      .on('value', (snapshot) => {
        const posts = snapshot.val();
        if (this._isMounted) {
          this.setState({ posts: posts });
        }
      });
  };
  handleSubmitPost = (e) => {
    e.preventDefault();
    const uid = fire.auth().currentUser.uid;
    const time = new Date().toLocaleString();
    firebaseDatabaseRef.ref('/posts').child(uid).push({
      uid: uid,
      postTime: time,
      displayName: fire.auth().currentUser.displayName,
      photoURL: fire.auth().currentUser.photoURL,
      post: this.state.newPost,
    });
    this.setState({ newPost: '' });
  };
  handleInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  render() {
    return (
      <div className={classes.Container}>
        <div className={classes.Post}>
          <div className={classes.PostForm}>
            <form onSubmit={this.handleSubmitPost}>
              <input
                type='text'
                name='newPost'
                placeholder='Hey there! Write Your Post...'
                value={this.state.newPost}
                onChange={this.handleInput}
                required
                autoComplete='true'
              />
              <button type='submit'>
                <GiPaperPlane style={{ color: 'white' }} />
              </button>
            </form>
          </div>
        </div>
        <div>
          {this.state.posts ? (
            Object.keys(this.state.posts).map((pos, i) => {
              return Object.keys(this.state.posts[pos])
                .reverse()
                .map((k, id) => {
                  return (
                    <Post
                      key={id}
                      showRemoveBtn={false}
                      timeAndDate={this.state.posts[pos][k].postTime}
                      profilePic={this.state.posts[pos][k].photoURL}
                      displayName={this.state.posts[pos][k].displayName}
                      post={this.state.posts[pos][k].post}
                    />
                  );
                });
            })
          ) : (
            <div className={classes.NoPostsCard}>Write Your Posts...</div>
          )}
        </div>
      </div>
    );
  }
}

export default Posts;
