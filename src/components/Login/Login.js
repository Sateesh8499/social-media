import React, { Component } from 'react';
import classes from './Login.module.css';
import fire from '../../config/Fire';
import { NavLink } from 'react-router-dom';
import { FiUser } from 'react-icons/fi';
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      havingError: false,
    };
  }
  handleInputChange = (event) => {
    this.setState({ havingError: false });
    this.setState({ [event.target.name]: event.target.value });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    fire
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(
        (res) => {
          window.location.href = '/home';
        },
        (error) => {
          this.setState({ havingError: true });
        }
      );
  };
  render() {
    return (
      <div
        className={classes.Login}
        style={{ backgroundColor: 'rebeccapurple' }}
      >
        <form className={classes.Form} onSubmit={this.handleSubmit}>
          <div style={{ justifyContent: 'center', display: 'flex' }}>
            <div className={classes.UserIcon}>
              <FiUser />
            </div>
          </div>
          <h2 className={`${classes.Header} 'text-center p-4'`}>Login</h2>
          {this.state.havingError ? (
            <div className={classes.havingError}>
              <span>Please Enter Valid Email and Password.</span>
            </div>
          ) : null}

          <div className='form-group'>
            <label htmlFor='email'>Email</label>
            <input
              type='text'
              name='email'
              value={this.state.email}
              onChange={this.handleInputChange}
              className='form-control'
              id='email'
              placeholder='example@email.com'
              autoComplete='true'
            />
          </div>
          <div className='form-group'>
            <label htmlFor='password'>Password</label>
            <input
              type='password'
              name='password'
              value={this.state.password}
              onChange={this.handleInputChange}
              className='form-control'
              id='password'
              placeholder='Enter Password'
            />
          </div>
          <button type='submit'>Login</button>
          <div className={classes.DontHaveAcc}>
            <span>
              Don't Have Account ? <br />
              <NavLink to='/register'> Create One </NavLink>
            </span>
          </div>
        </form>
      </div>
    );
  }
}

export default Login;
