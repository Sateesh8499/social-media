import React, { Component } from 'react';
import fire from './config/Fire';
import Login from './components/Login/Login';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Switch from 'react-bootstrap/esm/Switch';
import Register from './components/Register/Register';
import Layout from './components/Layout/Layout';
import Posts from './components/User/Posts/Posts';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
    };
  }

  componentDidMount() {
    this.authListener();
  }
  authListener() {
    fire.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user });
        fire
          .storage()
          .ref('users/' + fire.auth().currentUser.uid + '/profile.jpg')
          .getDownloadURL()
          .then((imgURL) => {
            fire
              .auth()
              .currentUser.updateProfile({
                photoURL: imgURL,
              })
              .then((res) => {});
          });
        localStorage.setItem('currentUser', JSON.stringify(user));
      } else {
        this.setState({ user: null });
        localStorage.removeItem('currentUser');
      }
    });
  }

  render() {
    return (
      <Router>
        <Switch>
          <div>
            {this.state.user ? (
              <Layout />
            ) : (
              <div>
                <Route exact path='/' component={Login} />
                <Route path='/login' component={Login} />
                <Route path='/register' component={Register} />
              </div>
            )}
          </div>
        </Switch>
      </Router>
    );
  }
}

export default App;
